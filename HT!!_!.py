# Подключіться до API НБУ ( документація тут https://bank.gov.ua/ua/open-data/api-dev ),
# отримайте теперішній курс валют и запишіть його в TXT-файл в такому форматі:
# опціонально передбачте для користувача можливість обирати дату, на яку він хоче отримати курс
# P.S. За можливості зробіть все за допомогою ООП

import requests
import datetime


class NBU:
    def __init__(self):
        self.url = "https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json"
        self.exchange_rates = {}

    def get_exchange_rate(self, date=None):
        params = {'date': date} if date else {}
        response = requests.get(self.url, params=params)
        if response.status_code == 200:
            data = response.json()
            for currency in data:
                self.exchange_rates[currency['cc']] = currency['rate']
            return True
        return False

    def write_to_txt(self, filename):
        if self.exchange_rates:
            with open(filename, 'w') as file:
                file.write(f'Today date {datetime.date.today()}\n')
                for i, (currency, rate) in enumerate(self.exchange_rates.items(), start=1):
                    file.write(f'{i}. {currency} to UAH: {rate}\n')
            print(f'Success! Exchange rates are saved in file ---> {filename}')
        else:
            print('Try again!')


nbu = NBU()
date = input('Type please your date:\nExample: 20010101\n')
if nbu.get_exchange_rate(date):
    nbu.write_to_txt('exchange_rates.txt')
